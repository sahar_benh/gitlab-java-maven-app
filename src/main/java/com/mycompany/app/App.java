package com.mycompany.app;

import static java.lang.System.*;
import org.apache.log4j.Logger;

/**
 * Hello world!
 */
public class App
{

    private final String message = "Hello World!!";
    public static final String MSG1 = "one";
    public static final String MSG2 = "two";
    private static final Logger LOG = Logger.getLogger(App.class);

    public App() {
    }

    public static void main(String[] args) {
        App myApp = new App();
        try {
            System.out.println(myApp.getMessage());
        } catch (InterruptedException e) {
            LOG.error(e.getMessage());
            System.exit(-1);
        }
    }

    private final String getMessage(boolean isSync) throws InterruptedException {
        return message;
    }

    private final String getMessage() throws InterruptedException {
        return message;
    }


}
